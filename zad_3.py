# -*- coding: utf-8 -*-
"""
Created on Tue Dec 08 19:02:27 2015

@author: Filip
"""

import urllib
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2014&vrijemeDo=31.12.2014'


airQualityHR = urllib.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot(y='mjerenje', x='vrijeme');

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek



#Zad 3.1.
mjerenje_osijek2014 =  df['mjerenje']
print mjerenje_osijek2014
#print df

#Zad 3.2.

df_sort =df.sort(['mjerenje'])
high = df_sort.tail(3)
print high['vrijeme']

#Zad 3.3.
broj = []

for i in range(1,12):
    k = df[df.month == i].count()
    broj.append(k['month'])
    print k['month']
  

fig = plt.figure()
ax = plt.subplot(111)
ax.bar(range(len(broj)), broj)
plt.xlabel('Month')
plt.ylabel('Number of days')
plt.title('PM10 - 2014')

#Zad 3.4.

zimski =  df[df.month == 1]
ljetni = df[df.month == 8]

usporedba = df[(df.month == 1) | (df.month == 8)]
print usporedba

usporedba.boxplot(column = 'mjerenje', by = 'month')

#Zadatak 3.5.

radni = df[(df.dayOfweek == 1) | (df.dayOfweek == 2) | (df.dayOfweek == 3) | (df.dayOfweek == 4) | (df.dayOfweek == 5)]
vikend = df[(df.dayOfweek == 6) | (df.dayOfweek == 7)]




