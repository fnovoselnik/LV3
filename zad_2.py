# -*- coding: utf-8 -*-
"""
Created on Tue Dec 01 20:55:26 2015

@author: Filip
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')
print mtcars


#Zadatak 2.1
mtcars_sort = mtcars.sort(['cyl'], ascending = True)
mtcars_sort[['cyl', 'mpg']].plot(kind = 'bar')


#Zadatak 2.2
mtcars.boxplot(column = 'wt', by = 'cyl')
plt.ylabel('wt')


#Zadatak 2.3. Iz grafa se vidi da vecu potrosnju imaju auti s automatskim mjenjacem
mtcars.boxplot(column = 'mpg' , by = 'am')
plt.ylabel('mpg')


#Zadatak 2.4.

am1 = mtcars[mtcars.am == 1]
am2 = mtcars[mtcars.am == 0]

hq1  = am1.iloc[:, [4,7]]
hq2  = am2.iloc[:, [4,7]]

ax = hq1.plot()
hq2.plot(ax= ax)



