# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 08:56:51 2015

@author: Filip
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')
#print mtcars

#1
df =mtcars.sort(['mpg'])
print df.head(3)


#2
print mtcars[(mtcars.cyl == 8)].sort(['mpg']).car.tail(3)

#3
print mtcars[(mtcars.cyl == 6)].mean()

#4
print mtcars[(mtcars.cyl == 4) & ((mtcars.wt > 2.000) & (mtcars.wt < 2.200))].mean()

#5
new_mtcars = mtcars.groupby('am')
print new_mtcars.count()

#6
print mtcars[(mtcars.am == 0) & (mtcars.hp > 100)].car.count()

#7
mtcars['kg'] = (mtcars.wt/2.2044)*1000
print mtcars





